from django.urls import path
from projects.views import task_list

urlpatterns = [path("mine/", task_list, name="show_my_tasks")]
