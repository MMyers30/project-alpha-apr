from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .models import Task

# Create your views here.


@login_required
def task_list(request):
    tasks = Task.objects.filter()
    context = {
        "tasks_list": tasks,
    }
    return render(request, "projects/project_list.html", context)


@login_required
def view_task(request):
    if request.method == "POST":
        form = task_list(request.POST)
        if form.is_valid():
            task_list = form.save
            task_list = tasks.objects.filter()
            task_list.save()
            return redirect("home")

    context = {
        "task": task_list,
    }
    return render(request, "project_list.html", context)
