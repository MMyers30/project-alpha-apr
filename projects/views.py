from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from .models import Project
from .forms import ProjectForm


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "project_list": projects,
    }
    return render(request, "projects/project_list.html", context)


@login_required
def show_project(request, pk):
    project_instance = Project.objects.get(pk=pk)
    if request.method == "POST":
        project_instance.delete()
        return redirect("project_list.html")

    return render(request, "projects/detail.html")


@login_required
def create_project(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("project_list.html")
    else:
        form = ProjectForm()

    context = {"form": form}

    return render(request, "projects/create_project.html", context)
